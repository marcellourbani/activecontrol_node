import {
  mergeOptions,
  ActiveControlSoapClient
} from "@basistechnologies/activecontrol-soap-api"
import { NodeAPI } from "node-red"
import { AcConfigNode } from "../ac_config_definitions"
import {
  isTestResultsMessage,
  TestResultsNode,
  TestResultsNodeDef
} from "../enterTestResultsDefinitions"
import { errorText } from "./util"

module.exports = (RED: NodeAPI) => {
  function createTestResNode(
    this: TestResultsNode,
    nodeDef: TestResultsNodeDef
  ) {
    const node = this
    RED.nodes.createNode(node, nodeDef)
    this.activecontrol = RED.nodes.getNode(
      nodeDef.activecontrol
    ) as AcConfigNode
    node.on("input", async function (msg, send, done) {
      try {
        if (!node.activecontrol) throw new Error("Configuration node missing")
        if (!node.activecontrol.config)
          throw new Error("Configuration node not set up properly")
        if  (!isTestResultsMessage(msg))
          throw new Error("Message format not recognized")
          
        node.status({ fill: "blue", text: "Saving test results..." })
        const { url, password, systemnumber, username } = msg
        const config = {
          ...node.activecontrol.config,
          ...node.activecontrol.credentials
        }
        const options = mergeOptions(config, {
          url,
          password,
          systemnumber,
          username
        })

        const client = new ActiveControlSoapClient(options)
        const { message, resultsId } = await client.enterTestResults(
          msg.payload
        )
        const payload = {
          error: false,
          message,
          resultsId,
          taskId: msg.payload.XTaskid
        }
        send({ ...msg, payload })
        node.status("")
        done()
      } catch (error) {
        node.status({ fill: "red", text: errorText(error) })
        done(error)
      }
    })
  }
  // register the node handler
  RED.nodes.registerType("enter-test-results", createTestResNode)
}
