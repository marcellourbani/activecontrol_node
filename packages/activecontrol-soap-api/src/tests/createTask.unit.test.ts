import { config } from "dotenv"
import { readFileSync } from "fs"
import { env } from "process"
import { isTask, Task } from "../taskDefinitions"
import { v4 } from "uuid"
import { xmlSubNode } from "../soap"
import { parse } from "fast-xml-parser"
import { ActiveControlSoapClient } from "../index"
import { taskCreateToXml } from "../utils"

config()
const readConfigFromEnv = () => ({
  url: env.AC_SERVER || "",
  username: env.AC_USER,
  password: env.AC_PASSWORD,
  systemnumber: env.AC_SYSTEM_NUMBER || ""
})
const loadSampleTask = () => {
  try {
    const raw = readFileSync("src/tests/sampletask.json")
    const task = JSON.parse(raw.toString("utf8"))
    if (isTask(task)) return task
  } catch (error) {
    return
  }
}

const createClient = () => new ActiveControlSoapClient(readConfigFromEnv())

test("create task", async  () => {
  if (!env.AC_ENABLE_WRITE) return
  const taskData = await loadSampleTask()
  if (!taskData) return // only run if there is some sample data
  const client = createClient()
  taskData.REFERENCE = v4().substr(0,  20)

  const taskId = await client.createTask(taskData)
  expect(taskId).toMatch(/[0-9]+/)
  // read back
  const createdTask = await client.readTask({ XTaskid: taskId })
  expect(createdTask.description).toBe(taskData.description)
  expect(createdTask.PROJECTID).toBe(taskData.PROJECTID)
  expect(createdTask.GROUPID).toBe(taskData.GROUPID)
  expect(createdTask.REFERENCE).toBe(taskData.REFERENCE)
})

test("(de)serialize", () => {
  const rawTask = loadSampleTask()
  if(!rawTask) return
  const serialized = taskCreateToXml("01", rawTask)
  const xml = parse(serialized)
  const parsedTask = xmlSubNode(xml,"XTask")
  expect(`${parsedTask.Id}`).toBe(rawTask.ID)
  expect(parsedTask.Reference).toBe(rawTask.REFERENCE)
})


test("(de)serialize 2", () => {
  const rawTask: Partial<Task> = {
    ID: "someId",
    CF_500: "cf 500 value",
    REFERENCE: "Foo"
  }
  if (!rawTask) return
  const serialized = taskCreateToXml("01", rawTask)
  expect(serialized.search(rawTask.CF_500 || "")).toBeGreaterThan(0)
  const xml = parse(serialized)
  const parsedTask = xmlSubNode(xml, "XTask")
  expect(parsedTask.Id).toBe(rawTask.ID)
  expect(parsedTask.Cf500).toBe(rawTask.CF_500)
  expect(parsedTask.Reference).toBe(rawTask.REFERENCE)
})

test("parse xml response", () => {
  const xml = `<YTask><Id>10020101600000123456</Id>`
  const raw = parse(xml, { parseTrueNumberOnly: true })
  expect(raw.YTask.Id).toBe("10020101600000123456")
})