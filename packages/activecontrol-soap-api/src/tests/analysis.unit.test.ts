import { createClient, readEnv } from "./utils"

test("Start an analysis", async () => {
  const client = createClient()

  const result = await client.startAnalysis({
    XTaskid: readEnv("AC_APPROVAL_TASKID"),
    XTarget: readEnv("AC_APPROVAL_TARGET"),
    //@ts-ignore
    XLocation: readEnv("AC_APPROVAL_LOCATON"),
    XAnltypeid: "",
    XtRequest: []
  })

  expect(result).toBeDefined()
  expect(result.YAnalysisId.length).toBe(20)
})

test("read analysis results", async () => {
  const client = createClient()

  const result = await client.readAnalysisResults({
    XAnalysisId: readEnv("AC_APPROVAL_ANALYSISID"),
    XMaxlines: ""
  })

  expect(result).toBeDefined()
  expect(result.isRunning).toBe(false)
  expect(result.conflicts.length).toBeGreaterThan(1)
  expect(result.conflicts[0].Trkorr).toMatch(/^\w\w\wK/)
  expect(result.resultGroups.length).toBeGreaterThan(0)
  const col = result.resultGroups[0].columns[0].COLUMNNAME
  expect(col).toBeTruthy()
  expect(result.resultGroups[0].group).toMatch(/^\d+$/)
  expect(result.resultGroups[0].results.length).toBeGreaterThan(0)
  expect(result.resultGroups[0].results[0][col]).toBeTruthy()
})
