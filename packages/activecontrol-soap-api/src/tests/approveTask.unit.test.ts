import { parse } from "fast-xml-parser"
import { AnalysisMode, parseApproveStepResponse } from "../approvals"
import { stripEnvelope } from "../soap"
import { createClient, readEnv } from "./utils"

test("Approve a task step", async () => {
  const client = createClient()

  const result = await client.approveStep({
    XAnalysisMode: AnalysisMode.StopOnError,
    XReference: readEnv("AC_APPROVAL_REFERENCE"),
    XStep: readEnv("AC_APPROVAL_STEP")
  })

  expect(result).toBeDefined()
  expect(result.conflicts.length).toBeGreaterThan(0) // repeatable test works best with a conflict
  expect(result.messages.length).toBeGreaterThan(0) // repeatable test works best with a conflict
  expect(result.conflicts[0].Trkorr).toMatch(/^\w\w\wK[\d\w]+$/)
  expect(result.conflicts[0].AllowImport).toBe(true)
  expect(result.results.length).toBeGreaterThan(0)
  expect(result.results[0].Approved).toBe(false)
  expect(result.analysis.length).toBeGreaterThan(0)
  expect(result.analysis[0].columns.length).toBeGreaterThan(0)
})

function skipunlessbreakpoint(time1= new Date()) {
  // will always return false. Switch in debug to run tests
  const time2 = new Date()
  const diff = time2.getTime() - time1.getTime()
  return diff < 1000
}
test("Approve a task by id/location", async () => {
  if (skipunlessbreakpoint()) return // will only work if we cheat in the debugger, as requires manual setup
  const client = createClient()

  const result = await client.approveTask({
    XAnalysisId: readEnv("AC_APPROVAL_ANALYSISID"),
    //@ts-ignore
    XLocation: readEnv("AC_APPROVAL_LOCATON"),
    XTarget: readEnv("AC_APPROVAL_TARGET"),
    XTaskid: readEnv("AC_APPROVAL_TASKID"),
    XtRequest: []
  })
  expect(result.message).toBeDefined()
})

test("Fail to approve a task by id/location", async () => {
  if (skipunlessbreakpoint()) return // will only work if we cheat in the debugger, as might requires manual setup
  const client = createClient()
  try {
    await client.approveTask({
      XAnalysisId: readEnv("AC_APPROVAL_ANALYSISID"),
      //@ts-ignore
      XLocation: readEnv("AC_APPROVAL_LOCATON") === "I" ? "O" : "I",
      XTarget: readEnv("AC_APPROVAL_TARGET"),
      XTaskid: readEnv("AC_APPROVAL_TASKID"),
      XtRequest: []
    })
    fail("this was supposed to throw an exception!");
      
  } catch (error) {
    // expected! 
  }  
})

test("Refuse to approve a task by id/location if no analysis id is provided", async () => {
  if (skipunlessbreakpoint()) return // will only work if we cheat in the debugger, as might requires manual setup
  const client = createClient()
  try {
    await client.approveTask({
      XAnalysisId: "",
      //@ts-ignore
      XLocation: readEnv("AC_APPROVAL_LOCATON"),
      XTarget: readEnv("AC_APPROVAL_TARGET"),
      XTaskid: readEnv("AC_APPROVAL_TASKID"),
      XtRequest: []
    })
    fail("this was supposed to throw an exception!")
  } catch (error) {
    // expected!
  }
})

test("parse response", () => {
  const raw = `<soap-env:Envelope xmlns:soap-env="http://schemas.xmlsoap.org/soap/envelope/"> <soap-env:Header/> <soap-env:Body> <n0:_-bti_-teTaskApproveStepWsResponse xmlns:n0="urn:sap-com:document:sap:soap:functions:mc-style"> <YAnalysisResults/> <YtConflictRequests/> <YtMessages> <item> <Msgtyp>I</Msgtyp> <Msgid/> <Msgnum>000</Msgnum> <Message>Approved                                                                                                                                                     1 transports in ACD MU target 1 Outbox</Message> <Msgv1>1</Msgv1> <Msgv2/> <Msgv3/> <Msgv4/> <Exception/> </item> </YtMessages> <YtResults> <item> <Task>10018031900000013816</Task> <Reference>gitlab1</Reference> <Target>0027</Target> <Location>O</Location> <Problems/> <Canapprove/> <Approved>X</Approved> </item> </YtResults> </n0:_-bti_-teTaskApproveStepWsResponse> </soap-env:Body> </soap-env:Envelope>`
  const stripped = stripEnvelope(
    parse(raw, { parseTrueNumberOnly: true }),
    "n0:_-bti_-teTaskApproveStepWsResponse"
  )
  const result = parseApproveStepResponse(stripped)
  expect(result.analysis.length).toBe(0)
  expect(result.conflicts.length).toBe(0)
  expect(result.messages[0].Msgtyp).toBe("I")
  expect(result.results[0].Task).toBe("10018031900000013816")
})