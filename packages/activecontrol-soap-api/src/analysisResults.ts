import { parse } from "fast-xml-parser"
import { xmlArray } from "./soap"
import sortBy from "lodash.sortby"
const parseAnalysisResult = (res: any): AnalysisResults => {
  const { LOCKING = "", HEADER = "", DESC = "", GROUPID = 0 } = res
  const columns: Column[] = sortBy(
    xmlArray(res, "COLUMNS", "item"),
    c => c.COLUMNID
  )
  const results = xmlArray(res, "DATA", "item").map(i => xmlArray(i, "item"))
  const convertResults = (r: any) => {
    const row: any = {}
    for (const c of columns) row[c.COLUMNNAME] = r[c.COLUMNID - 1]
    return row
  }
  return {
    locking: LOCKING === "X",
    headerText: HEADER,
    description: DESC,
    group: GROUPID,
    columns,
    results: results.map(convertResults)
  }
}

export const parseAnalysisResults = (res: string) => {
  if (!res) return []
  const raw = xmlArray(parse(res), "asx:abap", "asx:values", "RESULTS", "item")
  return raw.map(parseAnalysisResult)
}

export interface AnalysisResults {
  locking: boolean
  headerText: string
  description: string
  group: number
  columns: Column[]
  results: any[]
}

export interface Column {
  COLUMNID: number
  COLUMNNAME: string
  COLUMNWIDTH: number
  COLUMNTYPE: Columntype
}

export enum Columntype {
  Form = "F",
  Task = "T",
  Unknown = ""
}
