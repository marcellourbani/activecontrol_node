import { Library, LibraryEntry } from "./library"

class LibraryTest extends Library {
  parseDirectoryContentsTest(allEntries: LibraryEntry[], path: string) {
    return super.parseDirectoryContents(allEntries, path)
  }
}

const allEntries: LibraryEntry[] = [
  {
    path: "flows.json",
    data: { meta: {}, body: {} }
  },
  {
    path: "settings.json",
    data: { meta: {}, body: {} }
  },
  {
    path: "sub1/file1.json",
    data: { meta: {}, body: {} }
  },
  {
    path: "sub1/sub12/file12.json",
    data: { meta: {}, body: {} }
  },
  {
    path: "sub11/flows.exe",
    data: { meta: {}, body: {} }
  },
  {
    path: "sub2/file2.json",
    data: { meta: {}, body: {} }
  },
  {
    path: "sub2/file22.json",
    data: { meta: {}, body: {} }
  },
  {
    path: "sub2/sub23/file23.json",
    data: { meta: {}, body: {} }
  }
]

test("Directory listing: root", () => {
  const lib = new LibraryTest()
  const result = lib.parseDirectoryContentsTest(allEntries, "")
  const expected = [
    { fn: "flows.json" },
    { fn: "settings.json" },
    "sub1",
    "sub2",
    "sub11"
  ]
  expect(result.sort()).toEqual(expected.sort())
})
test("Directory listing: sub1", () => {
  const lib = new LibraryTest()
  const result = lib.parseDirectoryContentsTest(allEntries, "sub1")
  const expected = [{ fn: "file1.json" }, "sub12"]
  expect(result.sort()).toEqual(expected.sort())
})
test("Directory listing: subsubfolder", () => {
  const lib = new LibraryTest()
  const result = lib.parseDirectoryContentsTest(allEntries, "sub1/sub12")
  const expected = [{ fn: "file12.json" }]
  expect(result.sort()).toEqual(expected.sort())
})
test("Directory listing: subsubfolder with slashes", () => {
  const lib = new LibraryTest()
  const result = lib.parseDirectoryContentsTest(allEntries, "/sub2/sub23/")
  const expected = [{ fn: "file23.json" }]
  expect(result.sort()).toEqual(expected.sort())
})
test("Directory listing: nonexisting path", () => {
  const lib = new LibraryTest()
  const result = lib.parseDirectoryContentsTest(allEntries, "DOESNOTEXIST")
  expect(result.length).toBe(0)
})
